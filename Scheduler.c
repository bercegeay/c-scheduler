#include <stdlib.h>
#include <stdio.h>

/*
First we have defined a type called status which enumerates the current status of a process.
Due to the scope of this program, this type goes largely unused and has been created 
to structure future development of this program.
*/
typedef enum { new, readyStatus, run, wait, complete } status;
//This type holds weight when it comes to determining the push flow of the process. 
typedef enum { fcfs, sjf, roundrobin } scheduleType;

//A Process structure is critical to developing a process scheduler
struct Process
{
   	int pId;
   	int arrival;
   	int cpuBurst;
   	int priority;
   	int completion;
   	int start;
	status status;
	struct Process *next;
};

//To order and organize the scheduling of these processes, a queue is a favorable choice
struct Queue
{
	struct Process *front;
	struct Process *last;
	//The queue must have a type so that it knows which kind of algorithm to implement
	scheduleType st;
	//In order to determine the position of a process, we define a variable that counts each cycle
	int cycle;
	unsigned int size;
};

//Initialization of a process array to handle input from a file
struct Process processes[20];
//Initialization of the different types of queues which follow the process life cycle
struct Queue runQ;
//Due to the scope of this program, the wait queue goes largely unused as well.
struct Queue waitQ;
struct Queue readyQ;
struct Queue terminateQ;
int numJobs;
double resp, turn, art, att;


/*
This method initializes a queue
Since there are three different methods of execution, each calling the same run, ready, and terminated queue,
we must set the values of they contain to zero upon initialization
*/
void initQ(struct Queue *q, scheduleType st) {
	q->front = NULL;
	q->last = NULL;
	q->st = st;
	q->size = 0;

	if(q == &runQ){
		q->cycle = 0;
	}
	else if(q == &terminateQ){
		resp = 0;
		turn = 0;
	}
}

/*
Here begins a lengthy push method which, predominantly, contains all the logic to accurately 
schedule an array of processes per each scheduling algorithm.
*/
void pushQ(struct Queue *q, struct Process returnQ, int quantum) {
	q->size++;
	int startCycle;

	//RUN QUEUE LOGIC
	if(q == &runQ){
		/*
		If we are scheduling according to the Round Robin algorithm, we need to compute the cycle of each 
		push based on the specified quantum, unless the process contains less burst than the quantum, 
		in which we would add the remaining burst to the cycle
		*/
		if(q->st == roundrobin){
			if(returnQ.cpuBurst - quantum < 0 || quantum > returnQ.cpuBurst){
				q->cycle = q->cycle + returnQ.cpuBurst;
			}
			else{
				q->cycle = q->cycle + quantum;
			}
		}

		
		if (q->front == NULL) {
			q->front = (struct Process *) malloc(sizeof(struct Process));
			q->front->pId = returnQ.pId;
			q->front->arrival = returnQ.arrival;
			q->front->priority = returnQ.priority;
			q->front->status = returnQ.status;
			q->front->next = NULL;
			q->last = q->front;

			/*
			If the scheduling algorithm is not round robin, we simply assign the current cycle as 
			the start time of the process as well as the completion time by adding the processes' burst time
			to that cycle.
			*/
			if(q->st != roundrobin){
				q->front->cpuBurst = returnQ.cpuBurst;
				q->front->start = q->cycle;
				q->front->completion = q->cycle + returnQ.cpuBurst;
			}
			else{
				q->front->start = returnQ.start;
				if(returnQ.cpuBurst - quantum < 0 || quantum > returnQ.cpuBurst){
					q->front->cpuBurst = 0;
				}
				else{
					q->front->cpuBurst = returnQ.cpuBurst - quantum;
				}
			}
		}
		//Simply a reiteration of the previous logic except for pushes made after the queue front
		else {
			q->last->next = (struct Process *) malloc(sizeof(struct Process));
			q->last->next->pId = returnQ.pId;
			q->last->next->arrival = returnQ.arrival;
			q->last->next->priority = returnQ.priority;
			q->last->next->status = returnQ.status;
			q->last->next->next = NULL;
			q->last = q->last->next;

			if(q->st != roundrobin){
				q->last->next->cpuBurst = returnQ.cpuBurst;
				q->front->start = q->cycle;
				q->last->next->completion = q->cycle + returnQ.cpuBurst;
			}
			else{
				q->last->next->start = returnQ.start;
				if(returnQ.cpuBurst - quantum < 0 || quantum > returnQ.cpuBurst){
						q->last->next->cpuBurst = 0;
				}
				else{
						q->last->next->cpuBurst = returnQ.cpuBurst - quantum;
				}
			}
		}

		if(q->st != roundrobin){ q->cycle = q->cycle + returnQ.cpuBurst;}
	}
	//READY & WAIT QUEUE LOGIC
	else if(q == &readyQ || q == &waitQ){
		int j;
		
		//This push only passes data from one queue to the next, no calculations are made

		if (q->front == NULL) {
			q->front = (struct Process *) malloc(sizeof(struct Process));
			q->front->pId = returnQ.pId;
			q->front->arrival = returnQ.arrival;
			q->front->cpuBurst = returnQ.cpuBurst;
			q->front->status = returnQ.status;
			q->front->completion = returnQ.completion;
			q->front->start = returnQ.start;
			q->front->next = NULL;
			q->last = q->front;
		}
		else {
			if(q-> st == sjf){
				if(q->last->cpuBurst > returnQ.cpuBurst){
					

					q->last->next = (struct Process *) malloc(sizeof(struct Process));
					q->last->next->pId = q->last->pId;
					q->last->next->arrival = q->last->arrival;
					q->last->next->cpuBurst = q->last->cpuBurst;
					q->last->next->status = q->last->status;
					q->last->next->completion = q->last->completion;
					q->last->next->start = q->last->start;

					q->last->pId = returnQ.pId;
					q->last->arrival = returnQ.arrival;
					q->last->cpuBurst = returnQ.cpuBurst;
					q->last->status = returnQ.status;
					q->last->completion = returnQ.completion;
					q->last->start = returnQ.start;
					q->last->next->next = NULL;

					q->last = q->last->next;
				}
				else{
					q->last->next = (struct Process *) malloc(sizeof(struct Process));
					q->last->next->pId = returnQ.pId;
					q->last->next->arrival = returnQ.arrival;
					q->last->next->cpuBurst = returnQ.cpuBurst;
					q->last->next->status = returnQ.status;
					q->last->next->completion = returnQ.completion;
					q->last->next->start = returnQ.start;
					q->last->next->next = NULL;
					q->last = q->last->next;
				}
			}
			else{
			q->last->next = (struct Process *) malloc(sizeof(struct Process));
			q->last->next->pId = returnQ.pId;
			q->last->next->arrival = returnQ.arrival;
			q->last->next->cpuBurst = returnQ.cpuBurst;
			q->last->next->status = returnQ.status;
			q->last->next->completion = returnQ.completion;
			q->last->next->start = returnQ.start;
			q->last->next->next = NULL;

			q->last = q->last->next;
			}
		}
	}
	//TERMINATED QUEUE LOGIC
	else if(q == &terminateQ){
		int j;
		/*
		When a process enters the terminated queue, it's start and completion times have already been computed
		Next we subtract the arrival time of that process from each and add it to a global variable for each
		which help to arrive at the summation of all the processes.
		*/
		resp = resp + (returnQ.start - returnQ.arrival);
		turn = turn + (returnQ.completion - returnQ.arrival);

		if (q->front == NULL) {
			q->front = (struct Process *) malloc(sizeof(struct Process));
			q->front->pId = returnQ.pId;
			q->front->arrival = returnQ.arrival;
			q->front->cpuBurst = returnQ.cpuBurst;
			q->front->status = returnQ.status;
			q->front->completion = returnQ.completion;
			q->front->start = returnQ.start;
			q->front->next = NULL;
			q->last = q->front;
		}
		else {
			q->last->next = (struct Process *) malloc(sizeof(struct Process));
			q->last->next->pId = returnQ.pId;
			q->last->next->arrival = returnQ.arrival;
			q->last->next->cpuBurst = returnQ.cpuBurst;
			q->last->next->status = returnQ.status;
			q->last->next->completion = returnQ.completion;
			q->last->next->start = returnQ.start;
			q->last->next->next = NULL;
			q->last = q->last->next;
		}

	}

}

//POP LOGIC
struct Process popQ(struct Queue *q) {
	if(q == &runQ){
		q->size--;
		struct Process tran;
		tran.pId = q->front->pId;
		tran.arrival = q->front->arrival;
		tran.cpuBurst = q->front->cpuBurst;
		tran.priority = q->front->priority;
		tran.status = q->front->status;
		tran.start = q->front->start;
		
		/*
		The only discrepancy that exists in this pop is here
		If the scheduling algorithm is not Round Robin, we rely on previous logic to determine
		the completion time.
		Otherwise, the completion time is assigned the current cycle
		*/
		if(q->st != roundrobin){
			tran.completion = q->front->completion;
		}
		else{
			tran.completion = q->cycle;
		}

		q->front = q->front->next;

		return(tran);
	}
	else if(q == &readyQ || q == &waitQ){
		struct Process tran;
		tran.pId = q->front->pId;
		tran.arrival = q->front->arrival;
		tran.cpuBurst = q->front->cpuBurst;
		tran.priority = q->front->priority;
		tran.status = q->front->status;
		tran.completion = q->front->completion;
		tran.start = q->front->start;

		q->size--;

		q->front = q->front->next;

	return(tran);
	}
}

//Below are two helper functions which help us gather information about initialized queues
int getCycle(struct Queue *q){
	int j = q->cycle;
	return(j);
}

int size(struct Queue *q){
	int j = q->size;
	return(j);
}

//Here we determine the average response * turnaround times of the processes given the scheduling algorithm 
void calculate(){
	art = resp /(float)numJobs;
	att = turn /(float)numJobs;
}

void printResults(){
	printf("\n");
	printf("Run Stats \n");

	/* Print out answers. */
	printf("Average Response Time: ");
	printf("%f \n" , art);
	printf("Average Turnaround Time: ");
        printf("%f \n" , att);
	printf("------------------------------------------\n\n");
}


int loadJobs(char *filename)
{
	FILE *jobs;
	char string[80];
	int pId, arrival, cpuBurst, priority;
	int j;
	int nJobs;

	/* Open file of jobs to be put in the ready que. */
	jobs = fopen(filename, "r");

	/* Load the ready que from the file. */
	fgets(string, 80, jobs);
	printf("%s \n", string);
	j= 0;
	while(fscanf(jobs, "%d %d %d %d", &pId, &arrival, &cpuBurst, &priority) != EOF) {
		processes[j].pId = pId;
		processes[j].arrival = arrival;
		processes[j].cpuBurst = cpuBurst;
		processes[j].priority = priority;
		processes[j].status = new;

		printf("%d		%d	%d		%d \n", processes[j].pId, processes[j].arrival, processes[j].cpuBurst,
			                                processes[j].priority);
		j = j+1;
	}
	nJobs = j;
	printf("\n");
	printf("number of proccesses = %d \n", nJobs);
	numJobs = nJobs;
	fclose(jobs);

	return nJobs;
}

void firstServe() {
	initQ(&runQ, fcfs);
	initQ(&terminateQ, fcfs);

	printf("------------------------------------------\n\n");
        printf("            First Come First Serve\n\n");

   	int j;
	int jobs = numJobs;

	for(j=0;j<jobs;j++){
		pushQ(&runQ, processes[j], 0);
		processes[j] = popQ(&runQ);
		printf("%d ", processes[j].pId);
		printf(" start: ");
		printf("%d ", processes[j].start);
		printf(" complete: ");
		printf("%d \n", processes[j].completion);

		pushQ(&terminateQ, processes[j], 0);
	}

	calculate();
	printResults(&terminateQ);
   	//showRunStats(numJobs);
}

void Sjf(){
	int k;
	struct Process temp;

	initQ(&runQ, sjf);
	initQ(&readyQ, sjf);
	initQ(&terminateQ, sjf);

        printf("             Shortest Job First \n\n");

	for(k=0; k<numJobs; k++){
		pushQ(&readyQ, processes[k], 0);
	}

	for(k=0; k<numJobs; k++){
		temp = popQ(&readyQ);
		pushQ(&runQ, temp, 0);
		temp = popQ(&runQ);
		printf("%d ", temp.pId);
		printf(" start: ");
		printf("%d ", temp.start);
		printf(" complete: ");
		printf("%d \n", temp.completion);
		pushQ(&terminateQ, temp, 0);
	}

	calculate();
	printResults(&terminateQ);
}

void roundRobin(int quantum){
	int j, burstLeft, cycle;
	int startMarks = 0;
	int jobs = numJobs;
	struct Process temp;

    printf("                Round Robin \n");
	printf("                  VERBOSE \n\n");

	initQ(&runQ, roundrobin);
	initQ(&readyQ, roundrobin);
	initQ(&waitQ, roundrobin);
	initQ(&terminateQ, roundrobin);

		processes[0].start=0;
		processes[0].status=wait;
		pushQ(&readyQ, processes[0], 0);

		j=1;
		while(jobs > 0){
				temp = popQ(&readyQ);
				printf("pId: ");
				printf("%d \n", temp.pId);

				burstLeft = temp.cpuBurst;

				printf("Process Burst: ");
                                printf("%d \n", temp.cpuBurst);
				printf("wait -> ready \n");
                                printf("ready -> run \n");
				pushQ(&runQ, temp, quantum);
				temp = popQ(&runQ);

				if(startMarks < (numJobs - 1)){
					processes[j].start = temp.completion;
					startMarks++;
					printf("Initial Push: ");
					printf("%d \n", processes[j].pId);
					pushQ(&readyQ, processes[j], 0);
					j++;
				}

				if(temp.cpuBurst <= 0){

					printf("... \n");
					printf("Running ");
					printf("%d ", burstLeft);
					printf("cycles \n");
					printf("... \n");
					printf("run -> terminated \n\n");
					printf("start: ");
					printf("%d \n", temp.start);
					printf("completion: ");
					printf("%d \n" , temp.completion);
                                	printf("-------------\n");
					pushQ(&terminateQ, temp, 0);
					jobs--;
				}
				else{
					printf("... \n");
					printf("Running ");
					printf("%d ", quantum);
					printf("cycles \n");
					printf("... \n");
					printf("run -> wait \n");
					printf("wait -> ready \n");

					if(startMarks < (numJobs - 1) ){
						pushQ(&waitQ, temp, 0);
					}
					printf("-------------\n");
				}

				if(startMarks >= (numJobs - 1)) {
					int check = size(&waitQ);
					if(check != 0){
						temp = popQ(&waitQ);
						pushQ(&readyQ, temp, 0);
					}
				}
		}

	printf("------------------------------------------\n");
        printf("Round Robin \n");

	calculate();
	printResults(&terminateQ);

}

int main(void) {
	printf("--------------------------------------------------------\n");
	printf("                     W E L C O M E                      \n");
	loadJobs("./jobs.txt");

	firstServe();

	Sjf();

	roundRobin(15);

}
